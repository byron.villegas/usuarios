package cl.villegas.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import cl.villegas.dto.PhoneRequestDTO;
import cl.villegas.dto.UserRequestDTO;
import cl.villegas.exception.BusinessException;
import cl.villegas.service.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UserTest {
    private static final Logger logger = LoggerFactory.getLogger(UserTest.class);

    private UserRequestDTO userRequestDTO;

    @Autowired
    UserServiceImpl service;

    @Before
    public void setUp() {
        this.userRequestDTO = new UserRequestDTO();
    }

    @Test
    public void create() throws BusinessException {
        this.userRequestDTO.setName("Byron Villegas Moya");
        this.userRequestDTO.setEmail("byron.villegas@hotmail.cl");
        this.userRequestDTO.setPassword("Admin123");
        this.userRequestDTO.setPhones(Arrays.asList(new PhoneRequestDTO("123456789", "2", "56")));
        service.save(this.userRequestDTO);
        logger.info("Usuario creado");
        assertEquals(service.findTopByOrderByIdDesc().getEmail(), this.userRequestDTO.getEmail());
    }

    @Test(expected = BusinessException.class)
    public void notCreatedByInvalidEmail() throws BusinessException {
        this.userRequestDTO.setName("Byron Villegas Moya");
        this.userRequestDTO.setEmail("byron.villegas");
        this.userRequestDTO.setPassword("Admin123");
        this.userRequestDTO.setPhones(Arrays.asList(new PhoneRequestDTO("123456789", "2", "56")));
        service.save(this.userRequestDTO);
    }

    @Test(expected = BusinessException.class)
    public void notCreatedByInvalidPassword() throws BusinessException {
        this.userRequestDTO.setName("Byron Villegas Moya");
        this.userRequestDTO.setEmail("byron.villegas");
        this.userRequestDTO.setPassword("adm");
        this.userRequestDTO.setPhones(Arrays.asList(new PhoneRequestDTO("123456789", "2", "56")));
        service.save(this.userRequestDTO);
    }

    @Test(expected = BusinessException.class)
    public void notCreatedByRepeatEmail() throws BusinessException {
        this.userRequestDTO.setName("Byron Villegas Moya");
        this.userRequestDTO.setEmail("byron.villegas@hotmail.cl");
        this.userRequestDTO.setPassword("Admin123");
        this.userRequestDTO.setPhones(Arrays.asList(new PhoneRequestDTO("123456789", "2", "56")));
        service.save(this.userRequestDTO);
    }

    @Test
    public void isNotEmpty() {
        assertNotEquals(0, service.findAll().size());
    }

    @After
    public void tearDown() {
        this.userRequestDTO = null;
    }
}