package cl.villegas.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import cl.villegas.util.RegexValidatorUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class RegexValidatorTest {

    @Before
    public void setUp() {

    }

    @Test
    public void isValidEmail() {
        assertEquals(true, RegexValidatorUtil.validateEmail("byronvillegasm@gmail.com"));
    }

    @Test
    public void isNotValidEmail() {
        assertEquals(false, RegexValidatorUtil.validateEmail("byron.villegas"));
    }

    @Test
    public void isValidPassword() {
        assertEquals(true, RegexValidatorUtil.validatePassword("Admin123"));
    }

    @Test
    public void isNotValidPassword() {
        assertEquals(false, RegexValidatorUtil.validatePassword("a123"));
    }

    @After
    public void tearDown() {

    }
}