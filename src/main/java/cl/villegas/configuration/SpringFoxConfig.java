package cl.villegas.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import cl.villegas.enums.Swagger;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Profile("!production")
@Configuration
@EnableSwagger2
public class SpringFoxConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage(Swagger.PACKAGE.getValue())).build().apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(Swagger.TITLE.getValue()).description(Swagger.DESCRIPTION.getValue())
                .contact(new Contact(Swagger.NAME.getValue(), Swagger.URL.getValue(), Swagger.EMAIL.getValue()))
                .license(Swagger.LICENSE.getValue()).licenseUrl(Swagger.LICENSEURL.getValue())
                .version(Swagger.LICENSEVERSION.getValue()).build();
    }
}