package cl.villegas.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cl.villegas.enums.AES;

public class AESCrypto {
    private static final Logger logger = LoggerFactory.getLogger(AESCrypto.class);

    public static String encrypt(String value) {
        String encrypted = "";
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(
                    AES.INITVECTOR.getValue().getBytes(AES.CHARSET.getValue()));
            SecretKeySpec secretKeySpec = new SecretKeySpec(AES.KEY.getValue().getBytes(AES.CHARSET.getValue()),
                    AES.ENCRYPTTYPE.getValue());

            Cipher cipher = Cipher.getInstance(AES.ALGORITHM.getValue());
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] encryptedBytes = cipher.doFinal(value.getBytes());

            encrypted = DatatypeConverter.printBase64Binary(encryptedBytes);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            logger.error(ex.getMessage());
        }
        return encrypted;
    }

    public static String decrypt(String encrypted) {
        String decrypted = "";
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(
                    AES.INITVECTOR.getValue().getBytes(AES.CHARSET.getValue()));
            SecretKeySpec secretKeySpec = new SecretKeySpec(AES.KEY.getValue().getBytes(AES.CHARSET.getValue()),
                    AES.ENCRYPTTYPE.getValue());

            Cipher cipher = Cipher.getInstance(AES.ALGORITHM.getValue());
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] original = cipher.doFinal(DatatypeConverter.parseBase64Binary(encrypted));

            decrypted = new String(original);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            logger.error(ex.getMessage());
        }
        return decrypted;
    }
}