package cl.villegas.enums;

public enum AES {
    /**
     * Llave para la encriptacion {@code BVillegasMExpKey}.
     */
    KEY("BVillegasMExpKey"),
    /**
     * Vector para la encriptacion {@code VillegasInVector}.
     */
    INITVECTOR("VillegasInVector"),
    /**
     * Tipo de encriptacion {@code AES}.
     */
    ENCRYPTTYPE("AES"),
    /**
     * Algoritmo de encriptacion/desencriptacion de claves {@code AES/CBC/PKCS5PADDING}.
     */
    ALGORITHM("AES/CBC/PKCS5PADDING"),
    /**
     * Charset de la llave y el vector {@code UTF-8}.
     */
    CHARSET("UTF-8");

    private final String value;

    private AES(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}