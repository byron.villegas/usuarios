package cl.villegas.enums;

public enum Swagger {
    /**
     * Package base para los controladores {@code cl.villegas}.
     */
    PACKAGE("cl.villegas"),
    /**
     * Titulo para la documentacion {@code USUARIOS API}.
     */
    TITLE("USUARIOS API"),
    /**
     * Descripcion para la documentacion {@code Usuarios Api}.
     */
    DESCRIPTION("Usuarios Api"),
    /**
     * Nombre del contacto {@code Byron Stevens Villegas Moya}.
     */
    NAME("Byron Stevens Villegas Moya"),
    /**
     * URL del contacto {@code https://gitlab.com/byron.villegas/usuarios}.
     */
    URL("https://gitlab.com/byron.villegas/usuarios"),
    /**
     * Correo del contacto {@code byronvillegasm@gmail.com}.
     */
    EMAIL("byronvillegasm@gmail.com"),
    /**
     * Licencia de la documentacion {@code Apache 2.0}.
     */
    LICENSE("Apache 2.0"),
    /**
     * Url de la licencia {@code http://www.apache.org/licenses/LICENSE-2.0.html}.
     */
    LICENSEURL("http://www.apache.org/licenses/LICENSE-2.0.html"),
    /**
     * Version de la licencia {@code 1.0.0}.
     */
    LICENSEVERSION("1.0.0");

    private final String value;

    private Swagger(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}