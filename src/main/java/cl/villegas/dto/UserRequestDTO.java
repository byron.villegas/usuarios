package cl.villegas.dto;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class UserRequestDTO {
    @ApiModelProperty(notes = "Nombre")
    private String name;
    @ApiModelProperty(notes = "Email")
    private String email;
    @ApiModelProperty(notes = "Password encriptada")
    private String password;
    @ApiModelProperty(notes = "Telefonos")
    private List<PhoneRequestDTO> phones;
}