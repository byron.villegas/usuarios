package cl.villegas.dto;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class UserReponseDTO {
    @ApiModelProperty(notes = "UUID")
    private String id;
    @ApiModelProperty(notes = "Fecha creacion")
    private Timestamp created;
    @ApiModelProperty(notes = "Fecha modificacion")
    private Timestamp modified;
    @ApiModelProperty(notes = "Fecha ultimo login")
    private Timestamp lastLogin;
    @ApiModelProperty(notes = "Token")
    private String token;
    @ApiModelProperty(notes = "Activo")
    private boolean isActive;
}