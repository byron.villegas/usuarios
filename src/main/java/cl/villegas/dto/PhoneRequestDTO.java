package cl.villegas.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PhoneRequestDTO {
    @ApiModelProperty(notes = "Numero")
    private String number;
    @ApiModelProperty(notes = "Codigo ciudad")
    private String cityCode;
    @ApiModelProperty(notes = "Codigo pais")
    private String countryCode;
}