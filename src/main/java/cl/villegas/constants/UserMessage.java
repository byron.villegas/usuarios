package cl.villegas.constants;

public class UserMessage {
    public static final String INVALID_EMAIL = "Formato de correo incorrecto. Debe ser aaaaaaa@dominio.cl";
    public static final String INVALID_PASSWORD = "Formato de password incorrecto. Debe ser una mayuscula, letras minusculas y dos numeros";
    public static final String USER_ALREADY_CREATED = "El correo ya está registrado";
}