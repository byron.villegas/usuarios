package cl.villegas.exception;

import java.time.LocalDateTime;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import cl.villegas.dto.ResponseDTO;

@RestControllerAdvice(basePackages = { "cl.villegas.controller" })
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    ResponseDTO handleBadRequest(HttpServletRequest request, Exception ex) {
        logger.info(String.format("Excepcion: %s - %s", ex.getMessage(), LocalDateTime.now().toString()));
        return new ResponseDTO(ex.getMessage());
    }
}