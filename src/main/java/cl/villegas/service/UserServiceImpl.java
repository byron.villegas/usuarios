package cl.villegas.service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.villegas.constants.UserMessage;
import cl.villegas.dto.PhoneRequestDTO;
import cl.villegas.dto.UserReponseDTO;
import cl.villegas.dto.UserRequestDTO;
import cl.villegas.exception.BusinessException;
import cl.villegas.model.Phone;
import cl.villegas.model.User;
import cl.villegas.repository.UserRepository;
import cl.villegas.security.AESCrypto;
import cl.villegas.security.JwtToken;
import cl.villegas.util.RegexValidatorUtil;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    public void delete(User user) {
        if (user != null)
            repository.delete(user);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public User findTopByOrderByIdDesc() {
        return repository.findTopByOrderByIdDesc();
    }

    @Override
    public User findById(long id) {
        if (repository.findById(id).isPresent())
            return repository.findById(id).get();
        else
            return null;
    }

    private User generateByUserResponseDTO(UserRequestDTO userRequestDTO) {
        // Creacion objeto de negocio
        User user = new User();
        user.setName(userRequestDTO.getName());
        user.setEmail(userRequestDTO.getEmail());
        user.setPassword(AESCrypto.encrypt(userRequestDTO.getPassword()));
        user.setCreated(Timestamp.from(Instant.now()));
        user.setModified(Timestamp.from(Instant.now()));
        user.setLastLogin(Timestamp.from(Instant.now()));
        user.setIsActive(true);
        user.setUuid(UUID.randomUUID().toString());
        user.setToken(jwtToken.generateToken(user));
        user.setPhones(new ArrayList<Phone>());
        for (PhoneRequestDTO phone : userRequestDTO.getPhones()) {
            user.getPhones().add(new Phone(0, phone.getNumber(), phone.getCityCode(), phone.getCountryCode()));
        }
        return user;
    }

    @Override
    public UserReponseDTO save(UserRequestDTO userRequestDTO) throws BusinessException {
        if (!RegexValidatorUtil.validateEmail(userRequestDTO.getEmail()))
            throw new BusinessException(UserMessage.INVALID_EMAIL);
        if (!RegexValidatorUtil.validatePassword(userRequestDTO.getPassword()))
            throw new BusinessException(UserMessage.INVALID_PASSWORD);
        if (repository.findByEmail(userRequestDTO.getEmail()) != null)
            throw new BusinessException(UserMessage.USER_ALREADY_CREATED);

        User user = generateByUserResponseDTO(userRequestDTO); // Genera el objeto de negocio con los atributos solicitados

        repository.save(user);

        // Salida
        UserReponseDTO userReponseDTO = new UserReponseDTO(user.getUuid(), user.getCreated(), user.getModified(),
                user.getLastLogin(), user.getToken(), user.getIsActive());
        return userReponseDTO;
    }
}