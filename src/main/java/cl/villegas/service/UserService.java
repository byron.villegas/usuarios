package cl.villegas.service;

import java.util.List;
import cl.villegas.dto.UserReponseDTO;
import cl.villegas.dto.UserRequestDTO;
import cl.villegas.exception.BusinessException;
import cl.villegas.model.User;

public interface UserService {
    void delete(User user);

    List<User> findAll();

    User findByEmail(String email);

    User findTopByOrderByIdDesc();

    User findById(long id);

    UserReponseDTO save(UserRequestDTO userRequestDTO) throws BusinessException;
}