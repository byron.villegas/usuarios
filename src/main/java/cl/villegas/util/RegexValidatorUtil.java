package cl.villegas.util;

import java.util.regex.Pattern;

public class RegexValidatorUtil {
    private static final Pattern emailPattern = Pattern.compile("\\b[\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b"); // \\b[\\w\\.-]+@[\\w\\.-]+\\.cl\\b para admitir solo dominio cl
    private static final Pattern passwordPattern = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$"); // [A-Z]{1}[a-z]{1,21}[0-9]{2} para permitir solo una Mayuscula, letras minusculas y dos numeros

    public static boolean validateEmail(String email) {
        return emailPattern.matcher(email).find();
    }

    public static boolean validatePassword(String password) {
        return passwordPattern.matcher(password).find();
    }
}