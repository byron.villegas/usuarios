# Usuarios
Proyecto ejercicio de usuarios

## **Getting Started** 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

**Existen diversas maneras de tener una copia del proyecto a continuación las siguientes:**


**A traves Git Bash:**

Abrir la consola de Git Bash en la carpeta donde desea que se cree el proyecto mediante el siguiente comando:

    git clone https://gitlab.com/byron.villegas/usuarios.git

**A traves de descarga:**

Dentro del proyecto en GitLab botón download, descargará el proyecto en un zip


**A traves de complemento Git dentro del IDE:**

Recomiendo ver una guía respectiva para el IDE utilizado


Mira **Deployment** para conocer como desplegar el proyecto.


### **Prerequisites** 📋

_Que cosas necesitas para instalar el software y como instalarlas_

**Especificaciones:**
* Java JDK 1.8
* Maven 3.6.3

| **Herramientas** |
| ------ |
|[Java](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)|
|[Maven](https://maven.apache.org/download.cgi)|

**NOTA:** Solo si el IDE utilizado no tiene el complemento

**IDE's:**
* Apache Netbeans 11
* Eclipse IDE 4.13
* IntelliJ IDEA 2019.3.3 - BUILD 193.6494.35
* Visual Studio Code 1.42

| **IDE** |
| ------ |
|[Apache Netbeans](https://netbeans.apache.org/download/index.html)|
|[Eclipse IDE](https://www.eclipse.org/downloads/)|
|[IntelliJ IDEA](https://www.jetbrains.com/es-es/idea/download/#section=windows)|
|[Visual Studio Code](https://code.visualstudio.com/download)|

Java - Debe crear las siguientes variables de entorno con la respectiva carpeta JDK de su equipo:

    JAVA_HOME: C:\Program Files\Java\jdk1.8.0_151
    PATH: C:\Program Files\Java\jdk1.8.0_151\bin

Maven - Debe crear las siguientes variables de entorno con la respectiva carpeta Maven de su equipo:

    MAVEN_HOME: C:\Program Files\apache-maven-3.6.3
    M2_HOME: C:\Program Files\apache-maven-3.6.3
    PATH: C:\Program Files\apache-maven-3.6.3\bin

### **Installing** 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Dí cómo será ese paso_

### **Docker**

Para dockerizar la aplicación y desplegarla en un contenedor docker debe seguir los siguientes pasos descritos.

Dentro de la raiz del proyecto, debe abrir una consola y ejecutar el siguiente comando:

    mvn clean package 

**NOTA:** Maven descargará y actualizará las dependencias del proyecto y finalmente creara el jar de la aplicación llamado usuarios.jar

**Dockerfile**

Imagen Java JDK 8

    # Java JDK 8
    FROM openjdk:8-jdk-alpine

Exponer el puerto 8080

    # Make port 8080 available to the world outside this container
    EXPOSE 8080

Referenciar el archivo jar usuarios.jar

    # Refer to Maven build -> finalName
    ARG JAR_FILE=target/usuarios.jar

Nos posicionamos en el directorio /opt/app

    # cd /opt/app
    WORKDIR /opt/app

Copiamos utilizando la referencia el archivo usuarios.jar a app.jar

    # cp target/usuarios.jar /opt/app/app.jar
    COPY ${JAR_FILE} app.jar

Finalmente ejecutamos el jar utilizando el comando java -jar

    # java -jar /opt/app/app.jar
    ENTRYPOINT ["java","-jar","app.jar"]

Ejemplo Dockerfile:

    # Java 8
    FROM openjdk:8-jdk-alpine

    # Make port 8080 available to the world outside this container
    EXPOSE 8080

    # Refer to Maven build -> finalName
    ARG JAR_FILE=target/usuarios.jar

    # cd /opt/app
    WORKDIR /opt/app

    # cp target/usuarios.jar /opt/app/app.jar
    COPY ${JAR_FILE} app.jar

    # java -jar /opt/app/app.jar
    ENTRYPOINT ["java","-jar","app.jar"]


**Dockerizar la aplicación**

En la raiz del proyecto ejecutar el siguiente comando:

    sudo docker build -t usuarios:dev .

**docker build:** Comando utilizado para crear una imagen docker a traves de un dockerfile.

**-t:** Parametro utilizado para asignarle un tag a la imagen en este caso dev.

**NOTA:** Se creara la imagen docker usuarios con tag dev.

Visualizar imagenes de docker

    sudo docker image ls

Correr la imagen en contenedor docker con el siguinte comando:

    sudo docker run -d -p 8080:8080 -t usuarios:dev

**docker run:** Comando utilizado para correr la imagen en un contenedor docker.

**-d:** Parametro utilizado para hacer que corra en segundo plano.

**-p:** Parametro utilizado para mapear los puertos de salida del contenedor a un puerto del host. (Host Port) 8080:8080 (Container Port)

**NOTA:** El puerto del contenedor puede ser cualquiera, por ejemplo 8080:9090 el puerto del host para visualizar la aplicación es 8080 pero el del contenedor es 9090.

**-t:** Parametro utilizado para indicar que la imagen tiene asignada un tag en este caso dev.

Visualizar contenedores de docker

    sudo docker container ls

_Finaliza con un ejemplo de cómo obtener datos del sistema o como usarlos para una pequeña demo_

## **Deployment** 📦

_Despliegue de la aplicación_

**Swagger UI:**

Para visualizar Swagger UI debe entrar a la siguiente URL: **http://localhost:8080/usuarios/swagger-ui.html**

**H2 Database:**

Para visualizar H2 Database debe entrar a la siguiente URL: **http://localhost:8080/usuarios/h2-console**

**User Name:** sa

**Password:** password

## **Built With** 🛠️
_A continuación las herramientas utilizadas para la construcción del aplicativo_

**Especificaciones:**
* Java 1.8
* Tomcat Embed Core 9.0.31

**Administrador de dependencias:**
* Maven 4.0

**Framework:**
* Spring Framework Boot 2.3.0 RELEASE
    * Spring Boot Web 2.3.0 RELEASE
    * Spring Boot Actuator 2.3.0 RELEASE
    * Spring Boot Data JPA 2.3.0 RELEASE
    * Spring Boot DevTools 2.3.0 RELEASE
    * Spring Boot Test 2.3.0 RELEASE

**Base de datos:**
* H2 Database 1.4.200

**Persistencia:**
* Hibernate 5.4.32.Final

**Pool de conexiones:**
* HikariCP 3.4.5

**Herramientas utilizadas:**
* JJWT 0.9.1
* Lombok 1.18.20
* Swagger2 2.9.2
* Swagger-ui 2.9.2
